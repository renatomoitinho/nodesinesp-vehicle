import mongoose from 'mongoose';

export default class MongoConnection {

    constructor(host = '127.0.0.1', port = 27017, dbname) {
        this.host = host;
        this.port = port;
        this.dbname = dbname;
        this.url = `mongodb://${this.host}:${this.port}/${this.dbname}`;
    }

    withCredentials(user, password) {
        this.url = `mongodb://${user}:${password}@${this.host}:${this.port}/${this.dbname}?authSource=admin`;

        return this;
    }

    on(event, action) {
        mongoose.connection.on(event, action);

        return this;
    }

    connect() {
        this.on('connected', () => console.log(`Mongoose default connection open to ${this.url}`))
            .on('disconnected', () => console.log(`Mongoose! Disconnected from ${this.url}`))
            .on('error', err => console.log(`Mongoose default connection error: ${err}`));

        return mongoose.connect(this.url, {
            useNewUrlParser: true
        });
    }
}
