import sinesp from 'sinesp-nodejs';
import VehicleDAO from '../dao/VehicleDAO';
import VehicleDTO from '../models/view/VehicleDTO';

/**
 * Service Class of Vehicle
 */
export default class VehicleService {

    constructor() {
        this.vehicleDAO = new VehicleDAO();
    }

    /**
     * get vehicle and insert in database
     * @returns {Promise} sucesso com VehicleDTO
     */
    getVehicle(plate) {

        return this.vehicleDAO.findPlate(plate)
            .then((vehicle) => {

                if (vehicle) {
                    return vehicle;
                }
                // Sinesp API https://cidadao.sinesp.gov.br/sinesp-cidadao/mobile/consultar-placa/v4
                return sinesp.consultaPlaca(plate)
                    .then(result => this.vehicleDAO.savePlate(result))
                    .catch((error) => {
                        console.error('Erro ao consultar placa', error);
                        throw new Error(error.mensagemRetorno);
                    });
            })
            .then(vehicle => new VehicleDTO(vehicle));
    }

    /**
     * remove vehicle
     * @returns {Promise}
     */
    removeVehicle(plate) {
        return this.vehicleDAO.deletePlate(plate);
    }
}
