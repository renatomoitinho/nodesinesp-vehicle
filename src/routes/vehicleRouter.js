import express from 'express';
import VehicleService from '../services/VehicleService';

const router = express.Router();
const vehicleService = new VehicleService();

const getParam = (plate) => {
    if (plate && plate.length !== 7) {
        throw new Error(`${plate} invalid plate!`);
    }
    return plate.toUpperCase();
};

const sendError = (res, msg) => res.status(400).json({ error: msg });

const tryGetPlate = (plate, response) => {
    try {
        return getParam(plate);
    } catch (error) {
        sendError(response, error.message);
        return null;
    }
};

/*
 vehicle router
*/

// Delete vehicle
router.delete('/:plate', (req, res) => {

    vehicleService.removeVehicle(tryGetPlate(req.params.plate, res))
        .then(() => res.sendStatus(200))
        .catch(error => sendError(res, error.message));
});

// Get vehicle
router.get('/:plate', (req, res) => {
    console.log('Buscando pela placa ', req.params.plate);
    vehicleService.getVehicle(tryGetPlate(req.params.plate, res))
        .then(vehicle => res.json(vehicle))
        .catch(error => sendError(res, error.message));
});


export default router;
