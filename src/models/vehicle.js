import mongoose from 'mongoose';

const { Schema } = mongoose;

// create a schema
const vehicleSchema = new Schema({
    codigoRetorno: String,
    mensagemRetorno: String,
    codigoSituacao: String,
    situacao: String,
    modelo: String,
    marca: String,
    cor: String,
    ano: String,
    anoModelo: String,
    placa: { type: String, required: true, unique: true },
    data: String,
    uf: String,
    municipio: String,
    chassi: String,
    dataAtualizacaoCaracteristicasVeiculo: String,
    dataAtualizacaoRouboFurto: String,
    dataAtualizacaoAlarme: String
});

// the schema is useless so far
// we need to create a model using it
const Vehicle = mongoose.model('Vehicle', vehicleSchema);

// make this available to our users in our Node applications
export default Vehicle;
