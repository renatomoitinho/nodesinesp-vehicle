/**
 * 
 * 
 */
export default class VehicleDTO {
    constructor(data) {
        this.model = data.modelo;
        this.brand = data.marca;
        this.year = data.ano;
        this.modelYear = data.anoModelo;
        this.plate = data.placa;
        this.chassi = data.chassi;
    }
}