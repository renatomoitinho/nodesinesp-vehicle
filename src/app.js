import mongoose from 'mongoose';
import express from 'express';
import timeout from 'connect-timeout';
import vehicleRouter from './routes/vehicleRouter';
import indexRouter from './routes/indexRouter';
import MongoConnection from './database/MongoConnection';

const env = (properties, value = null) => {
    return process.env[properties] || value;
}

// Constants
const PORT = 49160;
const HOST = '0.0.0.0';

// start mongo db
new MongoConnection(env('config.mongo.host'), env('config.mongo.port'), env('config.mongo.dbname'))
    .withCredentials(env('config.mongo.user'), env('config.mongo.password'))
    .connect()
    .then(() => console.log('you are connected to mongo.'))
    .catch((error) => {
        throw error;
    });

const app = express();

// timeout
app.use(timeout(60000)); // 1 minute
app.use((req, res, next) => {
    if (!req.timedout) {
        next()
    } else {
        res.status(408).json({
            error: 'request timeout'
        });
    }
});

// Vehicle API
app.use('/', indexRouter);
app.use('/v1/vehicle/', vehicleRouter);

// Start application
app.listen(PORT, HOST, () => {
    console.log(`Running on http://${HOST}:${PORT}`);
});