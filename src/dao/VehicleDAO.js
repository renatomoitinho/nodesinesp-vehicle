import Vehicle from '../models/vehicle';

/**
 * @author renatomoitinho
 */
export default class VehicleDAO {

    static get excludeProperties() {
        return '-_id -__v';
    }

    /**
     * @param plate
     * @returns {Promise}
     */
    deletePlate(plate) {
        return new Promise((resolve, reject) => {
            Vehicle.deleteOne({ placa: plate }, (error) => {
                if (error) {
                    console.error('Erro ao deletar registro', error);
                    reject(error);
                    return;
                }
                resolve();
            });
        });
    }

    /**
     * @param plate
     * @returns {Promise}
     */
    findPlate(plate) {
        return new Promise((resolve, reject) => {
            Vehicle.findOne({ placa: plate }, VehicleDAO.excludeProperties, (error, vehicle) => {
                if (error) {
                    console.error('Erro ao buscar registro', error);
                    reject(error);
                    return;
                }
                resolve(vehicle);
            });
        });
    }

    /**
     * @param vehicle
     * @returns {Promise}
     */
    savePlate(vehicle) {
        return new Promise((resolve, reject) => {
            Vehicle(vehicle).save((error) => {
                if (error) {
                    console.error('Erro ao savar registro', error);
                    reject(error);
                    return;
                }
                resolve(vehicle);
            });
        });
    }
}
