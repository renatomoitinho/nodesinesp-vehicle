FROM nginx:latest

MAINTAINER Renato Moitinho Dias

COPY /docker/conf/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80 443

ENTRYPOINT ["nginx"]

CMD ["-g", "daemon off;"]