## Mongo db
```
docker pull mongo
docker run -d --name some-mongo -e MONGO_INITDB_ROOT_USERNAME=stilly -e MONGO_INITDB_ROOT_PASSWORD=stilly -p 27017:27017 -v /home/renato/data:/data/db mongo

```

## App node consulta de placa

```
docker build -t renatomoitinho/nodesinesp-vehicle .
```

```
docker push renatomoitinho/nodesinesp-vehicle

```

```
docker run -p 49160:8080 -d --name nodesinesp --env-file env.list renatomoitinho/nodesinesp-vehicle
```

```
curl -v http://localhost:49160/v1/vehicle/LPW3555
```

# instalando hint

npm install eslint --save-dev
./node_modules/.bin/eslint --init
